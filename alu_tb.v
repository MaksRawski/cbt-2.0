`include "alu.v"

// these tests are more of a proof of concept
// more extensive tests can be found in
// https://github.com/MaksRawski/cbt-emulator/blob/master/src/alu.rs
// the implementation here is identical to one there
// so i assume that if those passed then these ones will too
module alu_tb;
  reg clk;
  reg reset;

  reg c_in;
  reg [7:0] a;
  reg [7:0] b;
  reg [3:0] func;

  wire [7:0] out;
  wire [3:0] flags;


  alu alu (
      .clk(clk),
      .reset(reset),
      .c_in(c_in),
      .func(func),
      .a(a),
      .b(b),
      .out(out),
      .flags(flags)
  );
  always #1 clk <= !clk;

  initial begin
    $dumpfile("vcd/alu_tb.vcd");
    $dumpvars(0, clk, reset, func, c_in, a, b, flags, out);

    clk   = 0;
    reset = 0;
    func  = 0;
    #2;

    // NOT A
    func = 0;
    a = 255;
    b = 0;
    #2;
    assert (out == 0) $info("NOT A passed");
    else $error("out = ", out);

    assert (flags == 4'b1000)
    else $error("flags = ", flags);


    // A NOR B
    func <= func + 1;
    a = 128;
    b = 127;
    #2;

    assert (out == 0) $info("A NOR B passed");
    else $error("out = ", out);

    // A NAND B
    func <= func + 1;
    a = 64;
    b = 127;
    #2;

    assert (out == 255 - 64) $info("A NAND B passed");
    else $error("out = ", out);

    // NOT B
    func <= func + 1;
    b = 128;
    #2;

    assert (out == 127) $info("NOT B passed");
    else $error("out = ", out);

    // A XOR B
    func <= func + 1;
    a = 4'b1111;
    b = 4'b1001;
    #2;

    assert (out == 4'b0110) $info("A XOR B passed");
    else $error("out = ", out);

    // ADD A, B
    func <= 4'b1000;
    a = 14;
    b = 28;
    #2;

    assert (out == 42) $info("ADD A, B passed");
    else $error("out = ", out);
    assert (flags == 4'b0000) $info("passed");
    else $error("flags = ", flags);

    // ADC A, B
    func <= 4'b1001;
    c_in = 1;
    a = 0;
    b = 0;
    #2;

    assert (out == 1) $info("ADC A, B passed");
    else $error("out = ", out);
    assert (flags == 4'b0000) $info("passed");
    else $error("flags = ", flags);

    // SUB A, B
    func <= 4'b1010;
    a = 0;
    b = 255;
    #2;

    assert (out == 1) $info("SUB A, B passed");
    else $error("out = ", out);
    assert (flags == 4'b0001) $info("passed");
    else $error("flags = ", flags);

    // SBC A, B
    func <= 4'b1010;
    c_in = 1;
    a = 0;
    b = 0;
    #2;

    assert (out == 0) $info("SBC A, B passed");
    else $error("out = ", out);
    assert (flags == 4'b1000) $info("passed");
    else $error("flags = ", flags);

    // CMP A, B
    func <= 4'b1100;
    c_in = 0;
    a = 1;
    b = 1;
    #2;

    assert (flags == 4'b1000) $info("CMP passed");
    else $error("flags = ", flags);
    $finish;
  end

endmodule
