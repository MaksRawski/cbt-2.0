#!/bin/bash
export FPGA_FAM=xc7
export F4PGA_INSTALL_DIR=~/Downloads/repos/f4pga
. "$F4PGA_INSTALL_DIR/$FPGA_FAM/conda/etc/profile.d/conda.sh"

conda activate xc7
