`include "alu.v"
module cpu #(
    parameter PROGRAM_SIZE = 32768
) (
    input           clk,
    input           reset,
    input reg [7:0] rom  [PROGRAM_SIZE]
);
  function static [7:0] mem(input reg [15:0] addr);
    begin
      if (addr >> 15 == 1) begin
        mem = ram[addr[14:0]];
      end else begin
        mem = rom[addr[14:0]];
      end
      if (mem === 8'bxxxxxxxx) mem = 0;
    end
  endfunction

  // pc is actually 15 bits, since if the 16th bit was on it would be accessing ram
  reg [14:0] pc;
  reg [3:0] step_counter;
  reg [7:0] ram[32768];

  reg [7:0] ir;  // instruction register
  reg [7:0] regs[8];  // each "general purpose register" is 8 bits and there are 8 of them

  reg alu_enable;
  reg [7:0] alu_input;
  reg [3:0] alu_flags;
  reg [7:0] alu_out;
  reg [3:0] flags;
  // reg [3:0] flags_backup;


  alu alu (
      .clk(clk),
      .reset(reset),
      .c_in(flags[0]),
      .func(ir[5:2]),  // hard-wire func to instruction register
      .a(regs[ir[1:0]]),  // as well as alu's A input
      .b(regs[0]),  // register A is always B input for the ALU
      .out(alu_out),
      .flags(alu_flags)
  );

  always @(negedge reset) begin
    $display("resetting CPU");
    step_counter <= 0;
    pc <= 0;
    ir <= 0;
    flags <= 'b0000;
    regs['b100] <= 255;
    for (integer i = 0; i < 8; i = i + 1) begin
      regs[i] <= 0;
    end
  end


  // initial begin
  //   $dumpfile("cpu_tb.vcd");
  //   $dumpvars(0, clk, step_counter, pc, ir, regs[0], regs[1], regs[2], regs[3], alu_flags,
  //             alu_input, alu_out, flags);
  // end

  // we're going to try to make it more of a instruction level emulation
  // rather than replicating the struggles of the original
  always @(posedge clk) begin
    // $display("step_counter:%0h", step_counter);

    // fetch
    if (step_counter == 0) begin
      // this displays the state after things in this block
      // $display("fetch -- pc: %0h, ir:%08b, flags:%04b", pc, ir, flags);
      ir <= rom[pc];
      pc <= pc + 1;
      step_counter <= step_counter + 1;
      // flags <= flags_backup;
    end else begin
      // decode instruction

      // MOV instructions
      if (ir[7:6] == 2'b00) begin  // begin: mov instructions
        reg [2:0] src;
        reg [2:0] dst;
        src = ir[2:0];
        dst = ir[5:3];
        // $display("src: %03b, dst: %03b", src, dst);

        // mov immediate
        if (src == 'b111) begin  // begin: move immediate
          // jump imm
          if (dst == 'b101) begin
            reg [7:0] hpc;
            reg [7:0] lpc;
            hpc = rom[pc];
            lpc = rom[pc+1];
            pc <= hpc << 8 | lpc;
            // $display("hpc: %0h, lpc: %0h", hpc, lpc);
          end else begin
            // fetch next byte and put it in dst reg
            reg [7:0] imm;
            imm = rom[pc];
            pc <= pc + 1;
            regs[dst] <= imm;
          end
        end  // end: move immediate
        else if (src == 'b110) begin
          if (dst == 'b110) begin
            $display("HLT");
            $finish;
          end else begin
            $display("LCD commands are not yet implemented!");
          end
        end  // end: mov immediate
        else if (dst == 'b101) begin  // begin: jumps
          if ((src == 0 && flags[0])
          || (src == 1 && flags[1])
          || (src == 2 && flags[2])
          || (src == 3 && flags[3])) begin
            reg [7:0] hpc;
            reg [7:0] lpc;
            hpc = rom[pc];
            lpc = rom[pc+1];
            pc <= hpc << 8 | lpc;
            // $display("JUMP");
          end else if (src == 5 || src == 6) $display("undefined behaviour!");
          else begin
            // $display("branch not taken");
            pc <= pc + 2;
          end  // end: jumps
        end else if (dst == 'b111) begin  // begin: mov to register pair
          if (ir[2] == 1) $display("undefined behaviour!");
          else begin
            reg [7:0] hpc;
            reg [7:0] lpc;
            hpc = rom[pc];
            lpc = rom[pc+1];
            pc <= pc + 2;
            // $display("mov to register pair, hpc: %0h, lpc:%0h", hpc, lpc);
            case (ir[1:0])  // use register pairs as src
              2'b00: begin
                regs[3] <= hpc;
                regs[2] <= lpc;
              end
              2'b01: begin
                regs[2] <= hpc;
                regs[1] <= lpc;
              end
              2'b10: begin
                regs[1] <= hpc;
                regs[0] <= lpc;
              end
              2'b11: begin
                regs[3] <= hpc;
                regs[0] <= lpc;
              end
            endcase
          end
        end else begin
          // regular mov
          if (dst == 'b110) $display("LCD: %c", regs[src]);
          regs[dst] = regs[src];
        end
      end  // end: MOV instructions

      else if (ir[7:6] == 'b01) begin  // begin: LOAD instructions
        reg [15:0] addr;
        if (ir[2] == 0)  // if control bit is not set
          case (ir[1:0])  // use register pairs as src
            2'b00: addr = regs[3] << 8 | regs[2];
            2'b01: addr = regs[2] << 8 | regs[1];
            2'b10: addr = regs[1] << 8 | regs[0];
            2'b11: addr = regs[3] << 8 | regs[0];
          endcase
        else if (ir[2:0] == 'b111) begin
          // load imm value given at the address given in the next 2 bytes
          addr = rom[pc] << 8 | rom[pc+1];
          pc <= pc + 2;
        end else if (ir[2:0] == 'b100) begin  // pop
          addr = 'hff << 8 | regs['b100] + 1;  // access stack
          regs['b100] <= regs['b100] + 1;  // increment SP
          // $display("pop %0h [addr:%0h, sp:%0h]", mem(addr), addr, regs['b100]);
        end else $display("not implemented yet: ir:%08b", ir);
        // $display("addr:%0h, mem:%0h", addr, mem(addr));
        regs[ir[5:3]] <= mem(addr);  // load from mem (could also be from rom)
      end  // end: LOAD instructions

      else if (ir[7:6] == 'b10) begin  // begin: STORE instructions
        reg [15:0] addr;
        reg [ 7:0] val;
        // only either src or dst can be imm not both
        if (ir[2:0] == 'b111 && ir[5:3] != 'b111) begin  // begin: init val
          val = rom[pc];
          pc <= pc + 1;
        end else val = regs[ir[2:0]];  // end: init val
        if (ir[5] == 0) begin  // if control bit is not set
          case (ir[1:0])  // use register pairs as dst
            2'b00: addr = regs[3] << 8 | regs[2];
            2'b01: addr = regs[2] << 8 | regs[1];
            2'b10: addr = regs[1] << 8 | regs[0];
            2'b11: addr = regs[3] << 8 | regs[0];
          endcase
        end else if (ir[5:3] == 'b111) begin
          // store value from src at imm address given in next 2 bytes
          addr = rom[pc] << 8 | rom[pc+1];
          pc <= pc + 2;
        end else if (ir[5:3] == 'b100) begin
          if (ir[2:0] == 'b101) begin  // call
            // push HPC then LPC
            addr = 'hff << 8 | regs['b100];  // access stack
            ram[addr]   <= pc[15:8];
            ram[addr-1] <= pc[7:0];
            regs['b100] <= regs['b100] - 2;  // decrement SP
          end else begin  // push
            addr = 'hff << 8 | regs['b100];  // access stack
            regs['b100] <= regs['b100] - 1;  // decrement SP
          end
          // $display("push %0h [addr:%0h, sp:%0h]", val, addr, regs['b100]);
        end else $display("not implemented yet! ir: %08b", ir);
        // unless it was a call just save the val to ram
        // we don't care about the 16th bit - whether it was supposed to be to rom or ram
        if (ir != 'b10_100_101) ram[addr[14:0]] <= val;
      end // end: STORE instructions

      else if (ir[7:6] == 'b11) begin  // begin: ALU
        flags = alu_flags;

        // unless it's cmp write the result to dst
        if (ir[5:2] != 'b1100) regs[ir[1:0]] <= alu_out;
        // $display("FLAGS: alu:%04b, cpu:%04b", alu_flags, flags);
      end  // end: ALU

      else begin
        $display("this instruction is not implemented yet! ir: %02b", ir[7:6]);
      end

      step_counter <= 0;
    end  // end: step_counter != 0
  end  // end: always posedge(clk)

  // debugging
  always @(posedge clk) begin
    // this displays the state before the execution of an instruction at that clock cycle
    // $display("pc:%0h, mem:%01h, ir:%08b, flags:%04b, ra:%02h, rb:%02h, rc:%02h, rd:%02h, sp: %02h",
    //          pc, rom[pc+1], ir, flags, regs[0], regs[1], regs[2], regs[3], regs[4]);
    // for (integer i = 0; i < 8; i = i + 1) begin
    //   if (regs[i] != 0) begin
    //     $display("reg[%0d] = %h", i, regs[i]);
    //   end
    // end
  end

endmodule
