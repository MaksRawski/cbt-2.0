module alu (
    input clk,
    input reset,
    input c_in,
    input [3:0] func,
    input [7:0] a,
    input [7:0] b,
    output reg [7:0] out,
    output reg [3:0] flags
);
  reg c;
  reg h;
  reg o;
  reg z;
  reg [8:0] acc;

  always @(negedge reset) begin
    $display("resetting ALU");
    c   = 0;
    h   = 0;
    o   = 0;
    z   = 0;
    acc = 0;
  end

  always @(posedge clk) begin
    case (func)
      // LOGICAL OPERATIONS
      4'b0000: begin  // NOT
        out = ~a;
      end
      4'b0001: begin  // A NOR B
        out = ~(a | b);
      end
      4'b0010: begin  // A NAND B
        out <= ~(a & b);
      end
      4'b0011: begin  // NOT B
        out <= ~b;
      end
      4'b0100: begin  // A XOR B
        out <= a ^ b;
      end
      4'b0101: begin  // A XNOR B
        out <= ~(a ^ b);
      end
      4'b0110: begin  // A AND B
        out <= a & b;
      end
      4'b0111: begin  // A OR B
        out <= a | b;
      end

      // ARITHMETIC OPERATIONS
      4'b1000: begin  // ADD A, B
        acc = a + b;
        out = acc;
        c   = (acc > 255);
        h   = (a | b) & 1 << 4 != acc & 1 << 4;
        o   = (a | b) & 1 << 7 != acc & 1 << 7;
      end
      4'b1001: begin  // ADC A, B
        acc = a + b + c_in;
        out = acc;
        c   = (acc > 255);
        h   = (a | b) & 1 << 4 != acc & 1 << 4;
        o   = (a | b) & 1 << 7 != acc & 1 << 7;
      end
      4'b1010: begin  // SUB A, B
        acc = a - b;
        out = acc;
        c   = (acc > 255);
        h   = (a ^ b) & 1 << 4 == acc & 1 << 4;
        o   = (a ^ b) & 1 << 7 != acc & 1 << 7;
      end
      4'b1011: begin  // SBC A, B
        acc = a - b + ~c_in;
        out = acc;
        c   = (acc > 255);
        h   = (a ^ b) & 1 << 4 == acc & 1 << 4;
        o   = (a ^ b) & 1 << 7 != acc & 1 << 7;
      end
      4'b1100: begin  // A cmp B
        acc = a - b;
        c   = (acc > 255);
        h   = (a ^ b) & 1 << 4 == acc & 1 << 4;
        o   = (a ^ b) & 1 << 7 != acc & 1 << 7;
        z   = acc == 0;
      end
      4'b1101: begin  // INC A
        acc = a + 1;
        out = acc;
        c   = (acc > 255);
        h   = (a | b) & 1 << 4 != acc & 1 << 4;
        o   = (a | b) & 1 << 7 != acc & 1 << 7;
      end
      4'b1110: begin  // DEC A
        acc = a - 1;
        out = acc;
        c   = (acc > 255);
        h   = (a | b) & 1 << 4 != acc & 1 << 4;
        o   = (a | b) & 1 << 7 != acc & 1 << 7;
      end
      4'b1111: begin  // SHL A
        acc = a << 1;
        out = acc;
        c   = (acc > 255);
        h   = (a | b) & 1 << 4 != acc & 1 << 4;
        o   = (a | b) & 1 << 7 != acc & 1 << 7;
      end
    endcase
    z = acc == 0;
    flags = c | h << 1 | o << 2 | z << 3;
  end
endmodule
