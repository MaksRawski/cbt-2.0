`include "cpu.v"

module top (
    input             clk_i,
    input  wire       btn  [0],
    output      [3:0] led
);
  localparam LOG2DELAY = 25;
  reg [LOG2DELAY:0] counter = 0;
  reg [7:0] rom[32];

  wire slow_clk = counter[LOG2DELAY];
  always @(posedge clk) begin
    counter <= counter + 1;
  end

  cbt cpu (
      .clk  (slow_clk),
      .reset(btn[0]),
      .rom  (rom)
  );

endmodule
