`include "cpu.v"
module cpu_tb;
  reg clk;
  reg reset;

  reg [7:0] rom[32];
  cpu #(
      .PROGRAM_SIZE(32)
  ) cpu (
      .clk  (clk),
      .reset(reset),
      .rom  (rom)
  );

  always #1 clk <= ~clk;

  initial begin
    reset <= 1;
    reset <= 0;
    $readmemh("hello_world.txt", rom);
    // #1 for (integer i = 0; i < 32; i = i + 1) $display("rom[%00d]:%00h", i, rom[i]);
    clk <= 1;
    // rom[0] <= 'b00_000_111;  // mov a, 14
    // rom[1] <= 14;
    // rom[2] <= 'b10_100_000;  // push a
    // rom[3] <= 'b10_100_111;  // push 0x2a
    // rom[4] <= 'h2a;
    // rom[5] <= 'b01_001_100;  // pop b
    // rom[6] <= 'b01_010_100;  // pop c
    // rom[7] <= 'b00_110_110;  // HLT
    #350;

    $finish;
  end

endmodule
